from django.contrib import admin
from django.urls import path, include
from apps.users.views import IndexView
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('apps.translation.urls')),
    path('', IndexView.as_view(), name='index'),
    path('phrase/', include('apps.translation.urls')),
    path('users/', include('apps.users.urls')),
    path('users/', include('django.contrib.auth.urls')),
    # path('app2/', include('apps.users.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
